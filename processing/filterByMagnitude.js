
var fs = require('fs');
//https://github.com/astronexus/HYG-Database
var STAR_ATTRIBUTES = [
    "starId",
    "hip",
    "henryDraper",
    "harvardRevised",
    "gliese",
    "bayerFlamsteed",
    "properName",
    "rightAscension",
    "declination",
    "parsecs",
    "pmRightAscension",
    "pmDeclination",
    "rv",
    "magnitude",
    "absoluteMagnitude",
    "spectralType",
    "colorIndex",
    "x",
    "y",
    "z",
    "vx",
    "vy",
    "vz"
];
var STAR_OUTPUT_ATTRIBUTES = [
    "starId",
    "properName",
    "magnitude",
    "spectralType",
    "colorIndex",
    "rightAscension",
    "declination"
];

// Get input: file name, and number of stars wanted


var getInput = function(){
    if (process.argv.length !== 5){
        showUsage();
        return;
    }
    return {
        starCsvFile: process.argv[2],
        numberOfStars: process.argv[3],
        outputCsvFile: process.argv[4]
    };
};

var showUsage = function(){
    var usage = [
        "To use:",
        "\t" + process.argv[0] + " " + process.argv[1] + " star-csv-file number-of-stars output-csv-file"
    ];
    console.log(usage.join("\n"));
};

/*
Load stars from csv file in format (hygxyz.csv, found in https://github.com/astronexus/HYG-Database)
StarID,HIP,HD,HR,Gliese,BayerFlamsteed,ProperName,RA,Dec,Distance,PMRA,PMDec,RV,Mag,AbsMag,Spectrum,ColorIndex,X,Y,Z,VX,VY,VZ
Assume there are no quotes in the file, so all commas are field separators.

The callback is called when this is done, callback(err,stars)
0
 */
var loadStars = function(filename,callback){
    var stars = [];
    fs.readFile(filename,function(err,data){
        if (err){
            callback(err);
        }
        var lines = data.toString().split(/\n/);
        stars = lines.map(function(line,index){
            var fields = line.split(",");
            if (fields.length <= 1){ //bad data
                return;
            }
            var star = mapArrayToAttributes(fields,STAR_ATTRIBUTES);
            star.magnitude = parseFloat(star.magnitude);
            return star;
        });
        stars = stars.slice(1); // get rid of first line
        callback(err,stars);
    });
};

var mapArrayToAttributes = function(array,attributes){
    var obj = {};
    attributes.forEach(function(attribute,index){
        obj[attribute] = array[index];
    });
    return obj;
};

var toCsv = function(star,attributes){
    return toArray(star,attributes).join(",");
};

var toArray = function(star,attributes){
    var values = [];
    attributes.forEach(function(attribute){
        values.push(star[attribute]);
    });
    return values;
};

var toJson = function(stars,attributes){
    var allStars = [];
    stars.forEach(function(star){
        allStars.push(toArray(star,attributes));
    });
    return JSON.stringify(allStars);
};

/*
callback(err) when done
 */
var dumpStars = function(stars,fileName,callback){
    console.log("number of stars to dump:" + stars.length);
    fs.writeFile(fileName,toJson(stars,STAR_OUTPUT_ATTRIBUTES),callback);
//    var starLines = stars.map(function(star){
//        return toCsv(star,STAR_OUTPUT_ATTRIBUTES);
//    });
//    fs.writeFile(fileName,starLines.join("\n"),callback);
};

// the processing!
var main = function(){
    var input = getInput();
    if (input){
        var stars = [];
        loadStars(input.starCsvFile,function(err,stars){
            if (err){
                console.log("Error:" + err);
            }
            console.log("stars loaded:" + stars.length);
            stars = stars.sort(function(a,b){
                if (a.magnitude < b.magnitude){
                    return -1;
                } else if (a.magnitude > b.magnitude){
                    return 1;
                } else {
                    return 0;
                }
            });
            stars = stars.slice(0,parseInt(input.numberOfStars));
            dumpStars(stars,input.outputCsvFile,function(err){
                if (err){
                    console.log("Error writing to file " + input.outputCsvFile);
                }
            });
        });
    }
};

main();